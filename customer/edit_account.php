<?php

  include("includes/db.php");

  $user = $_SESSION['customer_email'];

  $get_customer = "select * from customers where customer_email='$user'";

  $run_customer = mysqli_query($con, $get_customer);

  $row_customer = mysqli_fetch_array($run_customer);

  $c_id = $row_customer['customer_id'];
  $name = $row_customer['customer_name'];
  $email = $row_customer['customer_email'];
  $pass = $row_customer['customer_pass'];
  $country = $row_customer['customer_country'];
  $city = $row_customer['customer_city'];
  $contact = $row_customer['customer_contact'];
  $address = $row_customer['customer_address'];
  $image = $row_customer['customer_image'];

?>


          <form action="" method="post" enctype="multipart/form-data">

            <table align="center" width="750">

              <tr align="center">
                <td colspan="2"><h2>Update Your Account</h2></td>
              </tr>

              <tr>
                <td align="right">Your Name : </td>
                <td><input type="text" name="c_name" value="<?php echo $name; ?>"></td>
              </tr>

              <tr>
                <td align="right">Your Email : </td>
                <td><input type="text" name="c_email" value="<?php echo $email; ?>"></td>
              </tr>

              <tr>
                <td align="right">Your Password : </td>
                <td><input type="password" name="c_pass" value="<?php echo $pass; ?>"></td>
              </tr>

              <tr>
                <td align="right" style="vertical-align: bottom;">Your Image : </td>
                <td><input type="file" name="c_image"><img src="customer_images/<?php echo $image; ?>" width="50" height="50" /></td>
              </tr>

              <tr>
                <td align="right">Your Country : </td>
                <td>
                  <select name="c_country" disabled>
                    <option><?php echo $country; ?></option>
                    <option>Bangladesh</option>
                    <option>India</option>
                    <option>Pakistan</option>
                    <option>Sri Lanka</option>
                  </select>
                </td>
              </tr>

              <tr>
                <td align="right">Your City : </td>
                <td><input type="text" name="c_city" value="<?php echo $city; ?>"></td>
              </tr>

              <tr>
                <td align="right">Your Contact : </td>
                <td><input type="text" name="c_contact" value="<?php echo $contact; ?>"></td>
              </tr>

              <tr>
                <style type="text/css">
                td {
  
                    vertical-align: top;
                  }
                </style>
                <td align="right">Your Address : </td>
                <td><textarea cols="25" rows="5" name="c_address"><?php echo $address; ?></textarea></td>
              </tr>

              <tr align="center">
                <td colspan="2"><input type="submit" name="update" value="Update Account"></td>
              </tr>

            </table>

          </form>

<?php

if (isset($_POST['update'])) {

  $ip = getIp();

  $customer_id = $c_id;
  $c_name = $_POST['c_name'];
  $c_email = $_POST['c_email'];
  $c_pass = $_POST['c_pass'];

  $c_image = $_FILES['c_image']['name'];
  $c_image_tmp = $_FILES['c_image']['tmp_name'];

  //$c_country = $_POST['c_country'];
  $c_city = $_POST['c_city'];
  $c_contact = $_POST['c_contact'];
  $c_address = $_POST['c_address'];

  move_uploaded_file($c_image_tmp, "customer_images/$c_image");

  $update_c = "update customers set customer_name='$c_name', customer_email='$c_email', customer_pass='$c_pass', customer_city='$c_city', 
               customer_contact='$c_contact', customer_address='$c_address', customer_image='$c_image' where customer_id='$customer_id' ";

  $run_update = mysqli_query($con, $update_c);

  if ($run_update) {

    echo "<script>alert('Account Update Successful !')</script>";
    echo "<script>window.open('my_account.php', '_self')</script>";
  }

}

?>