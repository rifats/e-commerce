-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 19, 2018 at 05:43 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`user_id`, `user_email`, `user_pass`) VALUES
(1, 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(100) NOT NULL AUTO_INCREMENT,
  `brand_title` text NOT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_title`) VALUES
(1, 'HP'),
(2, 'Lenovo'),
(3, 'Dell'),
(4, 'Sony'),
(5, 'Asus'),
(6, 'Amazon'),
(7, 'Apple');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `p_id` int(100) NOT NULL,
  `ip_add` varchar(255) NOT NULL,
  `qty` int(100) NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`p_id`, `ip_add`, `qty`) VALUES
(7, '127.0.0.1', 1),
(11, '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(100) NOT NULL AUTO_INCREMENT,
  `cat_title` text NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Laptops'),
(2, 'Cameras'),
(3, 'Mobiles'),
(4, 'Computers'),
(5, 'Tablets'),
(22, 'Television'),
(23, 'Printer'),
(24, 'Headphone'),
(25, 'Modem');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(100) NOT NULL AUTO_INCREMENT,
  `customer_ip` varchar(255) NOT NULL,
  `customer_name` text NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_pass` varchar(100) NOT NULL,
  `customer_country` text NOT NULL,
  `customer_city` text NOT NULL,
  `customer_contact` varchar(255) NOT NULL,
  `customer_address` text NOT NULL,
  `customer_image` text NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_ip`, `customer_name`, `customer_email`, `customer_pass`, `customer_country`, `customer_city`, `customer_contact`, `customer_address`, `customer_image`) VALUES
(1, '127.0.0.1', 'Imran Hossain', 'imran@gmail.com', 'imran', 'Bangladesh', 'Dhaka', '01724171589', 'Gulshan ,Dhaka', 'C16.jpg'),
(2, '127.0.0.1', 'Shabaz Abdullah', 'shabaz@gmail.com', 'shabaz', 'Bangladesh', 'Chittagong', '01715566260', 'Chawkbazar ,Chittagong', 'C03.jpg'),
(4, '127.0.0.1', 'Shahariar Rifat', 'rifat@gmail.com', '1234', 'Bangladesh', 'Chittagong', '01686970212', 'Halishahor ,Chittagong', 'My PP Pic.jpg'),
(5, '127.0.0.1', 'Sina Ibn Amin', 'sina@gmail.com', 'jogot', 'Bangladesh', 'Chittagong', '01915734594', 'Boubazar ,Chittagong', 'sina.png'),
(6, '127.0.0.1', 'Towfiqul Islam', 'towfiq@gmail.com', 'towfiq', 'Bangladesh', 'Chittagong', '01557845536', 'New Market ,Chittagong', 'tofiq.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(255) NOT NULL AUTO_INCREMENT,
  `product_cat` int(100) NOT NULL,
  `product_brand` int(100) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_keywords` text NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_brand`, `product_title`, `product_price`, `product_desc`, `product_image`, `product_keywords`) VALUES
(5, 5, 6, 'Amazon Fire 7', 9000, '<div class="product-name" style="margin: 0px 0px 13px; padding: 0px; box-sizing: border-box; font-size: 12px; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;; outline: none !important;">\r\n<h2 style="margin: 0px; padding: 0px; outline: none !important; box-sizing: border-box; line-height: 1.1; color: rgb(0, 0, 0); font-family: &quot;Titillium Web&quot;; font-weight: 500; font-size: 20.0004px;">Amazon Fire 7 (Quad Core 1.3 GHz, 1GB RAM, 8GB Storage, 7 Inch Display) Black Tablet with Alexa</h2>\r\n</div>\r\n<p class="no-rating" style="margin: 0px; padding: 0px; outline: none !important; box-sizing: border-box; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">&nbsp;</p>', 'amazon-kindle.jpg', 'amazon,tablet'),
(6, 3, 2, 'Sony Bravia 32''', 9000, '<p>hjyhj</p>', 'surface-pro.jpg', 'sony,tv'),
(7, 1, 5, 'Asus X540YA', 23310, '<p style="text-align: left;">Its Asus ,new and fit for you</p>', 'asus_x540ya.jpg', 'asus,laptop,notebook'),
(8, 4, 2, 'Product 1', 27000, '<p>Product is good one&nbsp;</p>', 'hospital-management-software.jpg', 'lenevo'),
(9, 3, 6, 'Product 2', 20000, '<p>Product is not a bad one</p>', 'slide1.jpg', 'amazon,mobile'),
(10, 2, 1, 'Product 3', 84000, '<p>Product 3 is expensive</p>', 'slide3.jpg', 'camera'),
(11, 1, 1, 'Product 4', 8000, '<p>Very Fine product</p>', 'Darksiders WrathofWar.jpg', 'laptop, hp');
