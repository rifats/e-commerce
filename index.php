<?php

session_start();

include("functions/functions.php");

?>

<!DOCTYPE>
<html>
  <head>
     <title>My Online Shop</title>

     <link rel="stylesheet" href="styles/style.css" media="all">
  </head>
<body>
   <!--Main Container starts here-->	
   <div class="main_wrapper">

      <!--Header starts here-->
      <div class="header_wrapper">
        <a href="index.php">
      	   <img id="logo" src="images/logo.jpg" width="500px" height="100px"/>
        </a>
      	<img id="banner" src="images/ad_banner.png" width="500px" height="100px"/>
      </div>
      <!--Header ends here-->


      <!--Nevigation Bar starts here-->
      <div class="menubar">
        	
        	<ul id="menu">
        		<li><a href="index.php">Home</a></li>
        		<li><a href="all_products.php">All Products</a></li>
            
            <?php

            if(isset($_SESSION['customer_email'])){
                echo "<li><a href='customer/my_account.php'>My Account</a></li>";
              }

            ?>

            <li><a href="#">Sign Up</a></li>
            <li><a href="cart.php">Shopping Cart</a></li>
            <li><a href="#">Contact Us</a></li>
        	</ul>

        	<div id="form">
        		<form action="results.php" method="get" enctype="multipart/form-data">
        			<input type="text" name="user_query" placeholder="Search a product ...">
        			<input type="submit" name="search" value="Search">
        		</form>
        	</div>

      </div>
      <!--Nevigation Bar ends here-->


      <!--Content Wrapper starts here-->
      <div class="content_wrapper">
        <div id="sidebar">
        	<div id="sidebar_title">Categories</div>
            
            <ul id="cats">
            	<?php getCats(); ?>
            </ul>

            <div id="sidebar_title">Brands</div>
            
            <ul id="cats">
            	<?php getBrands(); ?>
            </ul>
        </div>

        <div id="content_area">

          <?php cart(); ?>

          <div id="shopping_cart">
            <span style="float:right; font-size:18px; padding:5px; line-height:40px;">

              <?php

              if(isset($_SESSION['customer_email'])){
                echo "<b>Welcome </b>" . $_SESSION['customer_email'] . "<b style='color:yellow;'> Your </b>";
              }
              else{
                echo "<b>Welcome Guest </b>" . "<b style='color:yellow;'>Your </b>";
              }

              ?>

              <b style="color:yellow">Shopping Cart -</b> 
              Total Items: <?php total_items(); ?> &nbsp
              Total Price: <?php total_price(); ?> tk &nbsp
              <a href="cart.php" style="color:yellow">Go to Cart</a>

              <?php

              if (!isset($_SESSION['customer_email'])) {
                
                echo "<a href='checkout.php' style='color:orange;'>Login</a>";
              }
              else{
                echo "<a href='logout.php' style='color:white;'>Logout</a>";
              }

              ?>

            </span>
          </div>

          <?php $ip = getIp(); ?>

          <div id="products_box">
            
            <?php getPro(); ?>
            <?php getCatPro(); ?>
            <?php getBrandPro(); ?>

          </div>

        </div>
      </div>
      <!--Content Wrapper ends here-->


      <div id="footer">

        <h2 style="text-align:center; padding-top:30px">&copy; by ShahariarRifat</h2>

      </div>

   </div>
   <!--Main Container ends here-->
</body>
</html>